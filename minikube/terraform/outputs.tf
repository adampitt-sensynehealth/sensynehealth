output "minikube_pip" {
  value = "${var.minikube_on == 0 ? "Minikube is turned off" : element(concat(google_compute_instance.minikube-vm.*.network_interface.0.access_config.0.nat_ip, list("")), 0)}"
}
output "ssh_user" {
  value = "${var.ssh_user}"
}
output "ssh_pub_key" {
  value = "${var.ssh_pub_key}"
}
output "ansible_cmd" {
  value = "ansible-playbook -i ${element(concat(google_compute_instance.minikube-vm.*.network_interface.0.access_config.0.nat_ip, list("")), 0)} ../ansible/playbooks/minikube.yml"
}