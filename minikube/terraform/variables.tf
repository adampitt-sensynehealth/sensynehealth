variable "project_id" {}

variable "region" { default = "europe-west1" }
variable "zone" { default = "europe-west1-b" }

variable "minikube_on" {default = 1}

variable "workstation_cidr" {}
variable "ssh_user" {}
variable "ssh_pub_key" {}