resource "google_service_account" "minikube" {
  account_id   = "minikube"
  display_name = "minikube"
}

resource "google_compute_instance" "minikube-vm" {
  count = "${var.minikube_on}"

  name         = "minikube-vm"
  machine_type = "n1-standard-1"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7-v20181210"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.minikube.self_link}"

    access_config {
      // Ephemeral IP
    }
  }

  metadata {
    sshKeys = "${var.ssh_user}:${var.ssh_pub_key}"
  }

  service_account {
    email  = "${google_service_account.minikube.email}"
    scopes = []
  }

}

resource "null_resource" "ansible-minikube" {
  provisioner "local-exec" {
    command = "echo \"ansible command\""
  }
  depends_on = ["google_compute_instance.minikube-vm"]
}