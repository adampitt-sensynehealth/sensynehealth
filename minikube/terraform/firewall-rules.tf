resource "google_compute_firewall" "minikube-deny_all_ingress" {
  name        = "minikube-${var.project_id}-deny-all-ingress"
  network     = "${google_compute_network.minikube.self_link}"
  priority    = 65534
  direction   = "INGRESS"
  deny {
    protocol    = "all"
  }
}

resource "google_compute_firewall" "minikube-allow-ingress-from-workstation" {
  name        = "minikube-allow-ingress-from-workstation"
  network     = "${google_compute_network.minikube.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "tcp"
    ports = [22]
  }

  source_ranges = ["${var.workstation_cidr}"]
  target_service_accounts = ["${google_service_account.minikube.email}"]
}