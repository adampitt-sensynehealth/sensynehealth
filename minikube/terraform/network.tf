resource "google_compute_network" "minikube" {
  name                    = "minikube"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "minikube" {
  name          = "minikube"
  ip_cidr_range = "10.0.0.0/26"
  region        = "${var.region}"
  network       = "${google_compute_network.minikube.self_link}"
}

