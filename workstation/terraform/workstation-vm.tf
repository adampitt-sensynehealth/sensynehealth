resource "google_service_account" "workstation" {
  account_id   = "workstation"
  display_name = "workstation"
}

resource "google_compute_instance" "workstation-vm" {
  count = "${var.workstation_on}"

  name         = "workstation-vm"
  machine_type = "f1-micro"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7-v20181210"
    }
  }

  allow_stopping_for_update = true

  network_interface {
    subnetwork = "${google_compute_subnetwork.workstation.self_link}"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = <<SCRIPT
${file("${path.module}/install.sh")}
  SCRIPT

  metadata {
    sshKeys = "${var.ssh_user}:${var.ssh_pub_key}"
  }

  service_account {
    email  = "${google_service_account.workstation.email}"
    scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.full_control",
      "compute-rw",
      "storage-rw",
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

}