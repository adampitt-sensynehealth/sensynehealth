resource "google_project_iam_member" "project" {
  role    = "roles/owner"

  member = "serviceAccount:${google_service_account.workstation.email}"
}