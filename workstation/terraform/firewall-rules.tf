resource "google_compute_firewall" "workstation-deny_all_ingress" {
  name        = "workstation-${var.project_id}-deny-all-ingress"
  network     = "${google_compute_network.workstation.self_link}"
  priority    = 65534
  direction   = "INGRESS"
  deny {
    protocol    = "all"
  }
}

resource "google_compute_firewall" "workstation-allow-ingress-from-workstation" {
  name        = "workstation-allow-ingress-from-workstation"
  network     = "${google_compute_network.workstation.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "tcp"
    ports = [22]
  }

  source_ranges = ["${var.workstation_cidr}"]
  target_service_accounts = ["${google_service_account.workstation.email}"]
}