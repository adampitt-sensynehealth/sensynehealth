output "workstation_pip" {
  value = "${var.workstation_on == 0 ? "workstation is turned off" : element(concat(google_compute_instance.workstation-vm.*.network_interface.0.access_config.0.nat_ip, list("")), 0)}"
}
output "ssh_user" {
  value = "${var.ssh_user}"
}
output "ssh_pub_key" {
  value = "${var.ssh_pub_key}"
}