resource "google_compute_network" "workstation" {
  name                    = "workstation"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "workstation" {
  name          = "workstation"
  ip_cidr_range = "10.0.0.0/26"
  region        = "${var.region}"
  network       = "${google_compute_network.workstation.self_link}"
}

