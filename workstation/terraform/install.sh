#! /bin/bash
yum update
yum -y install git python-pip unzip
PIP_NO_INPUT=1
pip install ansible google-auth apache-libcloud
curl https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip > /tmp/terraform.zip
unzip /tmp/terraform.zip -d /opt
ln -s /opt/terraform /usr/local/bin
