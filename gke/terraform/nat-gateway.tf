resource "google_compute_router" "core-router" {
    name = "router"
    network = "${google_compute_network.core.name}"
}

resource "google_compute_address" "core-services-nat-ip" {
    name = "core-nat-ip"
}

resource "google_compute_router_nat" "core-services-nat" {
    name                               = "core-services-nat"
    router                             = "${google_compute_router.core-router.name}"
    nat_ip_allocate_option             = "MANUAL_ONLY"
    nat_ips                            = ["${google_compute_address.core-services-nat-ip.self_link}"]
    source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
    subnetwork {
        name = "${google_compute_subnetwork.gke.self_link}"
        source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
    }
}

resource "google_compute_global_address" "private_ip_alloc" {
    provider = "google-beta"
    name          = "privatenat"
    purpose       = "VPC_PEERING"
    address_type  = "INTERNAL"
    prefix_length = 16
    network       = "${google_compute_network.core.self_link}"
}

resource "google_service_networking_connection" "private_vpc_connection" {
    provider = "google-beta"
    network       = "${google_compute_network.core.self_link}"
    service       = "servicenetworking.googleapis.com"
    reserved_peering_ranges = ["${google_compute_global_address.private_ip_alloc.name}"]
}
