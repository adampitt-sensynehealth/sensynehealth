output "bastion_pip" {
  value = "${var.bastion_on == 0 ? "The Bastion is turned off" : element(concat(google_compute_instance.bastion.*.network_interface.0.access_config.0.nat_ip, list("")), 0)}"
}
output "bastion_user" {
  value = "${var.bastion_ssh_user}"
}
output "bastion_key" {
  value = "${var.bastion_ssh_pub_key}"
}
output "gcloud_k8s_command" {
  value = "gcloud container clusters get-credentials ${google_container_cluster.gke.name} --zone ${google_container_cluster.gke.zone} --project ${google_container_cluster.gke.project}"
}