resource "google_service_account" "bastion" {
  account_id   = "bastion"
  display_name = "bastion"
}

resource "google_project_iam_binding" "bastion-iam" {
  role = "roles/container.admin"
  members = ["serviceAccount:${google_service_account.bastion.email}"]
}

resource "google_project_iam_binding" "bastion-storage-iam" {
  role = "roles/storage.admin"
  members = ["serviceAccount:${google_service_account.bastion.email}", "serviceAccount:${google_service_account.gke.email}"]
}

resource "google_compute_instance" "bastion" {

  count = "${var.bastion_on}"

  name         = "${var.project_id}-bastion-vm"
  machine_type = "f1-micro"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7-v20181210"
    }
  }

  network_interface {
    subnetwork = "${google_compute_subnetwork.perimeter.self_link}"

    access_config {
      // Ephemeral IP
    }
  }

  metadata_startup_script = <<SCRIPT
${file("${path.module}/install.sh")}
usermod -aG docker ${var.bastion_ssh_user}
su - ${var.bastion_ssh_user} -c 'git clone https://adampitt-sensynehealth@bitbucket.org/adampitt-sensynehealth/sensynehealth.git /home/${var.bastion_ssh_user}/sensynehealth'
su - ${var.bastion_ssh_user} -c 'gcloud auth configure-docker --quiet'
su - ${var.bastion_ssh_user} -c 'docker build /home/${var.bastion_ssh_user}/sensynehealth/sensyne-flask-app -t eu.gcr.io/${var.project_id}/flask-app:0.3.0'
su - ${var.bastion_ssh_user} -c 'docker push eu.gcr.io/${var.project_id}/flask-app:0.3.0'
su - ${var.bastion_ssh_user} -c 'gcloud container clusters get-credentials ${var.project_id} --zone ${var.zone} --project ${var.project_id}'
su - ${var.bastion_ssh_user} -c 'kubectl create serviceaccount --namespace kube-system tiller'
su - ${var.bastion_ssh_user} -c 'kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller'
su - ${var.bastion_ssh_user} -c 'kubectl patch deploy --namespace kube-system tiller-deploy -p "{\"spec\":{\"template\":{\"spec\":{\"serviceAccount\":\"tiller\"}}}}"'
su - ${var.bastion_ssh_user} -c 'helm init --service-account tiller --upgrade'
su - ${var.bastion_ssh_user} -c 'helm dependency update /home/${var.bastion_ssh_user}/sensynehealth/gke/helm/sensyne-flask-app'
su - ${var.bastion_ssh_user} -c 'helm install /home/${var.bastion_ssh_user}/sensynehealth/gke/helm/sensyne-flask-app --set image.repository=${var.project_id}/flask-app'
SCRIPT

  metadata {
    sshKeys = "${var.bastion_ssh_user}:${var.bastion_ssh_pub_key}"
  }

  service_account {
    email  = "${google_service_account.bastion.email}"
    scopes = ["cloud-platform"]
  }

  allow_stopping_for_update = true

  depends_on = ["google_container_cluster.gke"]
}
