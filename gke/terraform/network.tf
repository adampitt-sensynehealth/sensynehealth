resource "google_compute_network" "core" {
  name                    = "core"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "perimeter" {
  name          = "perimeter"
  ip_cidr_range = "${var.perimeter_cidr}"
  region        = "${var.region}"
  network       = "${google_compute_network.core.self_link}"
  private_ip_google_access = true
}

resource "google_compute_subnetwork" "gke" {
  name          = "sensyne-gke-nodes"
  ip_cidr_range = "${var.k8s_node_cidr}"
  region        = "${var.region}"
  network       = "${google_compute_network.core.self_link}"
  private_ip_google_access = true
  secondary_ip_range {
    range_name    = "gke-pods"
    ip_cidr_range = "${var.k8s_pod_cidr}"
  }
    secondary_ip_range {
    range_name    = "gke-services"
    ip_cidr_range = "${var.k8s_svc_cidr}"
  }
}