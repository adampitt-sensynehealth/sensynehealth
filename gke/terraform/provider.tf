// gcloud auth application-default login
provider "google" {
  project     = "${var.project_id}"
  region      = "${var.region}"
}

provider "google-beta" {
  project     = "${var.project_id}"
  region      = "${var.region}"
}

//Using local is easier for now
//terraform {
//  backend "gcs" {
//    bucket  = ""
//    prefix  = ""
//  }
//}