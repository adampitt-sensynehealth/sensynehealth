resource "google_service_account" "gke" {
  account_id   = "sensyne-gke"
  display_name = "sensyne-gke"
}

resource "google_container_cluster" "gke" {
  name               = "${var.project_id}"
  zone               = "${var.zone}"
  initial_node_count = 2

  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  node_config {
    machine_type = "n1-standard-1"
    disk_size_gb = 10
    disk_type = "pd-standard"
    preemptible = true

    service_account = "${google_service_account.gke.email}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring"
    ]
  }

  network    = "${google_compute_network.core.name}"
  subnetwork = "${google_compute_subnetwork.gke.name}"

  ip_allocation_policy {
    cluster_secondary_range_name  = "gke-pods"
    services_secondary_range_name = "gke-services"
  }

  private_cluster_config {
    enable_private_endpoint = true
    enable_private_nodes    = true
    master_ipv4_cidr_block  = "${var.k8s_master_cidr}"
  }

  master_authorized_networks_config {
    cidr_blocks {
      display_name = "perimeter"
      cidr_block = "${var.perimeter_cidr}"
    }
  }

  lifecycle {
    ignore_changes = ["node_pool", "node_config", "master_authorized_networks_config", "network", "subnetwork"]
  }

}