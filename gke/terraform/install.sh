#! /bin/bash
yum update
yum -y install kubectl yum-utils device-mapper-persistent-data lvm2 git
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce

systemctl enable docker.service
systemctl start docker.service

curl https://storage.googleapis.com/kubernetes-helm/helm-v2.12.2-linux-amd64.tar.gz > /tmp/helm.tar.gz
tar -zxvf /tmp/helm.tar.gz --directory /tmp
mv /tmp/linux-amd64/helm /opt
mv /tmp/linux-amd64/tiller /opt
ln -s /opt/helm /usr/local/bin
ln -s /opt/tiller /usr/local/bin
