variable "project_id" {}

variable "region" { default = "europe-west1" }
variable "zone" { default = "europe-west1-b" }

variable "perimeter_cidr" { default = "10.0.0.0/28" }

variable "k8s_master_cidr" { default = "10.0.32.0/28" }
variable "k8s_node_cidr" { default = "10.0.64.0/26" }
variable "k8s_pod_cidr" { default = "10.0.128.0/18" }
variable "k8s_svc_cidr" { default = "10.0.72.0/21" }

variable "bastion_on" { default = 1 }
variable "workstation_cidr" {}
variable "bastion_ssh_user" {}
variable "bastion_ssh_pub_key" {}