//resource "google_compute_firewall" "deny_all_ingress" {
//  name        = "${var.project_id}-deny-all-ingress"
//  network     = "${google_compute_network.core.self_link}"
//  priority    = 65534
//  direction   = "INGRESS"
//  deny {
//    protocol    = "all"
//  }
//}
//
//resource "google_compute_firewall" "deny_all_egress" {
//  name        = "${var.project_id}-deny-all-egress"
//  network     = "${google_compute_network.core.self_link}"
//  priority    = 65534
//  direction   = "EGRESS"
//  deny {
//    protocol    = "all"
//  }
//}

resource "google_compute_firewall" "allow-ingress-from-workstation" {
  count = "${var.bastion_on}"
  name        = "allow-ingress-from-workstation"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "tcp"
    ports = [22]
  }

  source_ranges = ["${var.workstation_cidr}"]
  target_service_accounts = ["${google_service_account.bastion.email}"]
}

resource "google_compute_firewall" "allow-ingress-from-k8s-master" {
  count = "${var.bastion_on}"
  name        = "allow-ingress-from-k8s-master"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "all"
  }

  source_ranges = ["${var.k8s_master_cidr}"]
  target_service_accounts = ["${google_service_account.bastion.email}"]
}

resource "google_compute_firewall" "allow-egress-from-bastion" {
  count = "${var.bastion_on}"
  name        = "allow-egress-from-bastion"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "EGRESS"
  allow {
    protocol    = "all"
  }
  destination_ranges = ["0.0.0.0/0"]
  target_service_accounts = ["${google_service_account.bastion.email}"]
}

resource "google_compute_firewall" "allow-egress-from-gke" {
  count = "${var.bastion_on}"
  name        = "allow-egress-from-gke"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "EGRESS"
  allow {
    protocol    = "all"
  }
  destination_ranges = ["0.0.0.0/0"]
  target_service_accounts = ["${google_service_account.gke.email}"]
}

resource "google_compute_firewall" "allow-ingress-gke" {
  name        = "allow-ingress-gke"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "all"
  }

  source_service_accounts = ["${google_service_account.gke.email}"]
  target_service_accounts = ["${google_service_account.gke.email}"]
}

resource "google_compute_firewall" "allow-ingress-ssh-gke" {
  name        = "allow-ingress-ssh-gke"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "all"
  }

  source_service_accounts = ["${google_service_account.bastion.email}"]
  target_service_accounts = ["${google_service_account.gke.email}"]
}

resource "google_compute_firewall" "allow-ingress-gke-master" {
  name        = "allow-ingress-gke-master"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "INGRESS"
  allow {
    protocol    = "all"
  }

  source_ranges = ["${var.k8s_master_cidr}"]
  target_service_accounts = ["${google_service_account.gke.email}"]
}

resource "google_compute_firewall" "allow-egress-gke" {
  name        = "allow-egress-gke"
  network     = "${google_compute_network.core.self_link}"
  priority    = 100
  direction   = "EGRESS"
  allow {
    protocol    = "all"
  }

  target_service_accounts = ["${google_service_account.gke.email}"]
  destination_ranges = ["${var.k8s_node_cidr}", "${var.k8s_master_cidr}"]
}
