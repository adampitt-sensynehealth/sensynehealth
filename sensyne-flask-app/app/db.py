import psycopg2
from config import Config
import logging


LOGGER = logging.getLogger(__name__)


class Database:
    _user = Config.DATABASE_USER
    _password = Config.DATABASE_PASSWORD
    _host = Config.DATABASE_HOST
    _port = Config.DATABASE_PORT
    _database = Config.DATABASE_DATABASE

    def get_connection(self):
        connection = None
        cursor = None
        LOGGER.debug("Connecting to Database")
        try:
            connection = psycopg2.connect(user=self._user,
                                          password=self._password,
                                          host=self._host,
                                          port=self._port,
                                          database=self._database)
            cursor = connection.cursor()

            cursor.execute("SELECT * FROM information_schema.tables where table_name=%s", ('tags',))

            if not bool(cursor.rowcount):
                cursor.execute("""
                    CREATE TABLE tags
                    (
                      id bigint NOT NULL,
                      tag character varying COLLATE pg_catalog."default" NOT NULL,
                      CONSTRAINT tags_pkey PRIMARY KEY (id)
                    );

                    INSERT INTO tags(
                      id, tag)
                      VALUES (1, 'dog');
                """)
                connection.commit()

            return connection, cursor
        except (Exception, psycopg2.Error) as error:
            LOGGER.debug("Error while connecting to PostgreSQL", error)
            if connection:
                if cursor:
                    cursor.close()
                    LOGGER.debug("PostgreSQL cursor is closed")
                connection.close()
                LOGGER.debug("PostgreSQL connection is closed")
            raise error

    def get_tag(self):
        connection, cursor = self.get_connection()

        try:
            cursor.execute("SELECT tag FROM tags WHERE id = 1 ;")
            record = cursor.fetchone()
            return record[0]
        except (Exception, psycopg2.Error) as error:
            LOGGER.debug("Error while connecting to PostgreSQL", error)
            if connection:
                if cursor:
                    cursor.close()
                    LOGGER.debug("PostgreSQL cursor is closed")
                connection.close()
                LOGGER.debug("PostgreSQL connection is closed")
            raise error
        finally:
            cursor.close()
            connection.close()

    def set_tag(self, tag):

        connection, cursor = self.get_connection()

        try:
            cursor.execute("UPDATE tags SET tag = %s WHERE id = 1;", (tag,))

            connection.commit()
        except (Exception, psycopg2.Error) as error:
            LOGGER.debug("Error while connecting to PostgreSQL", error)
            if connection:
                if cursor:
                    cursor.close()
                    LOGGER.debug("PostgreSQL cursor is closed")
                connection.close()
                LOGGER.debug("PostgreSQL connection is closed")
            raise error
        finally:
            cursor.close()
            connection.close()
        return 200
