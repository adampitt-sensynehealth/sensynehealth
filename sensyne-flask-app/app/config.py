import os


class Config:
    API_KEY = os.environ['GIFY_API_KEY']
    DATABASE_HOST = os.environ['DATABASE_HOST']
    DATABASE_PORT = os.environ['DATABASE_PORT']
    DATABASE_DATABASE = os.environ['DATABASE_DATABASE']
    DATABASE_USER = os.environ['DATABASE_USER']
    DATABASE_PASSWORD = os.environ['DATABASE_PASSWORD']
