import logging
import requests


LOGGER = logging.getLogger(__name__)


def get_random_gify(api_key=None, tag="cat"):
    LOGGER.debug(f"Searching for random gify with tag {tag}")
    try:
        r = requests.get(
            f"https://api.giphy.com/v1/gifs/random?api_key={api_key}&rating=PG&tag={tag}")
        request_json = r.json()
        image_url = request_json.get('data', {}).get(
            'images', {}).get('original', {}).get("url", None)
        if not image_url:
            return request_json
        LOGGER.debug(f"Found random {tag.upper()} gif with url: {image_url}")
        return image_url
    except Exception as e:
        LOGGER.debug("Failed to get a url from gify")
        raise e
