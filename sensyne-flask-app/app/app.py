from flask import Flask, render_template, request
from config import Config
from gify import get_random_gify
from db import Database


app = Flask(__name__)
db = Database()


@app.route('/hello')
def hello():
    return "Hello World!"


@app.route('/')
def index():
    tag = db.get_tag()
    return render_template('gif.html', tag=tag, gify_reponse=get_random_gify(Config.API_KEY, tag))


@app.route('/tag', methods=['POST'])
def set_tag():
    new_tag = request.form.get('new_tag')
    db.set_tag(new_tag)
    return render_template('gif.html', tag=new_tag,  gify_reponse=get_random_gify(Config.API_KEY, new_tag))


if __name__ == '__main__':
    app.run(host='0.0.0.0')
