## Sensyne DevOps Engineer - Tech Test

### Tags

0.1.0 I wanted to demonstrate both terraform/ansible so built minikube. I went for the quickest implementation possible on it with pre-built docker images.

0.2.0 I hadn't made a Flask application before or used python for a while so gave it a shot and made sensyne-flask-app. I also quickly built my own workstation as I was using a windows machine at this point.

0.3.0 I thought I hadn't really displayed my knowledge of production deployments so thought I'd build a secured private cloud kubernetes cluster... problem is my app needed the internet so I ended up adding a NAT gateway and pretty much canning the firewall rules. But I did build a helm chart and automate the release.

In hindsight I think I should of just deployed a GKE cluster as fast as possible and demonstrated deploying applications and orechestrating them using ingress / stateful sets and other features.I should of deployed Jenkins and automated the deployment of a pre-built app.

Final note, please excuse the lack of code style, I tried to rush features in the time I had

### Pre-requisites

- Google Cloud SDK 229.0.0
- Terraform v0.11.11
- A Google Project (preferably new so no resource name clash)
- A public half of an SSH key in OpenSSH format (ssh-keygen -t rsa -b 4096)
- A Giphy API Key (https://developers.giphy.com/)

### Install/Deploy
```gcloud auth application-default login```
- Fill in the below config file and store it in a file named myvars.auto.tfvars inside gke/terraform
```file
project_id = "your-google-project-id"
workstation_cidr = "your.public.ipv4.ip/32"
bastion_ssh_user =  "yourname"
bastion_ssh_pub_key = "ssh-rsa AA.... your-key"
```
```cd gke/terraform```

```terraform init```

```terraform apply```

- Check and approve

- Load your private key into your ssh-agent

```ssh $(terraform output bastion_user)@$(terraform output bastion_pip)```

- Wait for automation scripts to run, Debug section below describes how to spectate scripts

```kubectl create secret generic flask-app --from-literal=GIFY_API_KEY=***```

```kubectl get svc```

- You can now see the NodePort e.g.  and hit that via an SSH tunnel to expose your application, I ran out of time to deploy an ingress / loadbalancer. The tunnel will need to go to one of the GKE nodes hostnames : the nodeport e.g gke-sensyne-health-12345-default-pool-f3bcd13a-hbs0:30048

### Debug

- Ensure LF line endings if issues with start-up script
- SSH to the bastion and run to debug the install script

```sudo journalctl -f -u google-startup-scripts.service```

### Discussions to be had

- Ingress controller / Http Load Balancer
- Security Controls (SSL, Network Policy ect)
- Build servers CI/CD
- Upgrade cycle
- Traffic management
- Monitoring
- Scaling / auto-scaling
- Secret management
- Logging




